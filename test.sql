﻿# Host: 127.0.0.1  (Version 8.0.22)
# Date: 2021-03-16 04:13:45
# Generator: MySQL-Front 6.0  (Build 2.20)


#
# Structure for table "tag_message"
#

DROP TABLE IF EXISTS `tag_message`;
CREATE TABLE `tag_message` (
  `Id` int NOT NULL DEFAULT '0',
  `tag` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

#
# Data for table "tag_message"
#

INSERT INTO `tag_message` VALUES (1,'ame_to_yuki'),(2,'ame_nochi_yuki'),(3,'eushully'),(4,'inui_sana'),(5,'izumi_tsubasu'),(6,'koiken_otome'),(7,'komeshiro_kasu'),(8,'kujou_danbo'),(9,'mashiroiro_symphony'),(10,'miyasaka_miyu'),(11,'miyasaka_nako'),(12,'pan_no_mimi'),(13,'smee'),(14,'yano_mitsuki'),(15,'kuze_matsuri'),(16,'korie_riko'),(17,'matsumiya_kiseri'),(18,'yuzu-soft'),(19,'shiratama'),(20,'nekono_shiro'),(21,'sanoba_witch'),(22,'senren_banka'),(23,'dracu-riot!'),(24,'natsuzora_kanata'),(25,'noble_works'),(26,'amairo_islenauts'),(27,'riddle_joker'),(28,'bra-ban!'),(29,'tenshinranman'),(30,'hinata_momo'),(31,'muku_(apupop)'),(32,'purin_purin'),(33,'purin_kai_yogurt'),(34,'suzunone_rena'),(35,'minatsuki_kou'),(36,'karenai_sekai_to_owaru_hana'),(37,'saga_planets'),(38,'pan'),(39,'sekai_seifuku_kanojo'),(40,'munemoto_tsubakiko'),(41,'hitokage'),(42,'oda_nobuna_no_yabou'),(43,'shika_yuno'),(44,'soul_worker'),(45,'tsukishima_yuuri'),(46,'hamikoron');
