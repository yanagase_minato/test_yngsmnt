package com.ssm.test;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.ssm.dao.tag_messageDao;
import com.ssm.model.tag_message;
import com.ssm.controller.tag_MessageController;
import com.ssm.service.tag_messageService;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring-mybatis.xml" })
public class TestMybatis {
    private static Logger logger = Logger.getLogger(TestMybatis.class);

    @Resource
    private tag_messageService con = null;

    @Test
    public void test1() {
 //	List<tag_message> tag_message = con.selectByPrimaryKey(1);
    	tag_message tag_message = con.selectByPrimaryKey(1);
       logger.info("select tag_message = " + tag_message.toString());
    }
}