package com.ssm.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ssm.model.tag_message;

public interface tag_messageDao {
    int deleteByPrimaryKey(Integer id);

    int insert(tag_message record);

    int insertSelective(tag_message record);

    tag_message selectByPrimaryKey(Integer id);
    
    List<tag_message> findtag_messageList(@Param("id")Integer id);
    
    int updateByPrimaryKeySelective(tag_message record);

    int updateByPrimaryKey(tag_message record);
}