package com.ssm.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import net.sf.json.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ssm.model.tag_message;
import com.ssm.service.tag_messageService;

@Controller
@RequestMapping
public class tag_MessageController {
    @Resource
    private tag_messageService tag_messageservice = null;

    /**
     * 查询列表
     */
    
    @RequestMapping(value="/list",method = RequestMethod.GET)
    public String list(Map<String,Object>map) {
    	List<tag_message> tag_message = tag_messageservice.findtag_messageList(null);
    	JSONArray json=JSONArray.fromObject(tag_message);
    	map.put("tag_messageLists", json);
      return "tag_messageLists";
    }
    
    /*
    @RequestMapping(value="/list",method = RequestMethod.GET)
    public @ResponseBody List<tag_message> list() {
    	List<tag_message> tag_message = tag_messageservice.findtag_messageList(null);
    	JSONArray json=JSONArray.fromObject(tag_message);
      return json;
    }
    
    @RequestMapping(value="/list",method = RequestMethod.GET)
    public String list(Map<String,Object>map) {
    	List<tag_message> tag_message = tag_messageservice.findtag_messageList(null);
    	JSONArray json=JSONArray.fromObject(tag_message);
    	map.put("angulartag", json);
      return "list";
    }
   
    @RequestMapping(value="/list", method=RequestMethod.GET)
    public String findtag_messageList(@ModelAttribute("tag_message")tag_message tag_message, ModelMap model) {
        List<tag_message> tag_messageLists = tag_messageservice.findtag_messageList(null);
        model.addAttribute("tag_messageLists", tag_messageLists);
        return "tag_messageLists";
    }

    @RequestMapping(value="/tag/" ,method=RequestMethod.GET)
    public ModelAndView queryList(HttpServletRequest request, HttpServletResponse response) {
    	//String id = request.getParameter("id");
    	Integer id=Integer.parseInt(request.getParameter("id"));
        List<tag_message> tag_messageLists = tag_messageservice.findtag_messageList(id);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("tag_messageLists", tag_messageLists);
        modelAndView.setViewName("tag_message");
        return modelAndView;
    }

    @RequestMapping(value="/tag/" ,method=RequestMethod.GET)
	public String list(Map<String,Object> map){
		List<tag_message> tag_message = tag_messageservice.findtag_messageList(null);
		 JSONArray json = new JSONArray();
         for(tag_message pLog : tag_message){
             JSONObject jo = new JSONObject();
             jo.put("id", pLog.getId());
             jo.put("tag", pLog.getTag());         
             json.add(jo); 
         }
		//map.put("user",user);	
        map.put("tags",json);
		return "list";
	}
    @RequestMapping(value="/tag/",method = RequestMethod.GET)
    public @ResponseBody List<tag_message> list() {
    	List<tag_message> tag_message = tag_messageservice.findtag_messageList(null);
    	JSONArray json=JSONArray.fromObject(tag_message);
      return json;
    }
    
    @RequestMapping(value="/list")
    public @ResponseBody List<tag_message> list() {
    	List<tag_message> tag_message = tag_messageservice.findtag_messageList(null);
    	JSONArray json=JSONArray.fromObject(tag_message);
      return json;
    }
    
   
    
    */
    /**
     * 查询TAG
     */
    @RequestMapping(value="/list/edit",method = RequestMethod.GET)
    public @ResponseBody tag_message edit(Integer id) {
        return tag_messageservice.selectByPrimaryKey(id) ;
    }
    
    /**
     * 新建TAG
     */
    @RequestMapping(value="/list/add", method=RequestMethod.POST)
    public @ResponseBody String add(@ModelAttribute("tag_message")tag_message tag_message) {
    	tag_messageservice.insert (tag_message);
        return "ok";
    }
    /**
     * 编辑TAG
     */
    @RequestMapping(value="/list/update", method=RequestMethod.POST)
    public @ResponseBody String update(@ModelAttribute("tag_message")tag_message tag_message) {
    	tag_messageservice.updateByPrimaryKey(tag_message);
        return "ok";
    }
    /**
     * 删除TAG
     */
    @RequestMapping(value="/list/delete", method=RequestMethod.DELETE)
    public @ResponseBody String delete(Integer id) {
        tag_messageservice.deleteByPrimaryKey(id);
        return "ok";
    }

}
