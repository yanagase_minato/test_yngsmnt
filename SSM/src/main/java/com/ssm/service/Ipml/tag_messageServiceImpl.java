package com.ssm.service.Ipml;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.ssm.dao.tag_messageDao;
import com.ssm.model.tag_message;
import com.ssm.service.tag_messageService;

@Service("tag_messageService")
public class tag_messageServiceImpl implements tag_messageService {
    // 定义DAO
    @Resource
    private tag_messageDao tag_messageDao = null;

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return tag_messageDao.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(tag_message record) {
        return tag_messageDao.insert(record);
    }

    @Override
    public int insertSelective(tag_message record) {
        return tag_messageDao.insertSelective(record);
    }

    @Override
    public tag_message selectByPrimaryKey(Integer id) {
        return tag_messageDao.selectByPrimaryKey(id);
    }


    @Override
    public List<tag_message> findtag_messageList(Integer id) {
        return tag_messageDao.findtag_messageList(id);
    }

    @Override
    public int updateByPrimaryKeySelective(tag_message record) {
        return tag_messageDao.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(tag_message record) {
        return tag_messageDao.updateByPrimaryKey(record);
    }
}