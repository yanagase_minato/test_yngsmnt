<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<% 
String path = request.getContextPath(); 
String basepath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/"; 
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular-resource.js"></script>
<link href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
 
<!-- jQuery文件。务必在bootstrap.min.js 之前引入 -->
<script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
 
<!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
<script src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>tag管理</title>
</head>
<body>
        
        
<div  ng-app="myApp" ng-controller="tagCtrl">       
 <a href="#" class="btn btn-info btn-xs" data-toggle="modal" data-target="#tagAddDialog")">添加tag</a>
<div class="input-group">
<span class="input-group-addon">查询</span>
<input tpye="text" class="form-control" placeholder="输入关键字" ng-model="test"></input>
</div>
        <table class="table table-borderd table-striped" border="1" cellpadding="10" cellspacing="0">
        <thead>
            <tr>
                <th>id列表</th>
                <th>tag列表</th>
                <th>管理</th>
            </tr>
</thead>
<tbody>
                <tr ng-repeat="x in tag_messagelist | filter:test">
                    <td>{{x.id}}</td>
                    <td>{{x.tag}}</td>
                    <td><a href="#" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#tagEditDialog" ng-click="edittag(x.id)">修改</a>
                    <a href="#" class="btn btn-danger btn-xs" ng-click="deletetag(x.id)">删除</a> </td>
                </tr>
                
   </tbody>
   
        </table>
        
	<div class="modal fade" id="tagEditDialog" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content" >
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">修改tag信息</h4>
				</div>
				<div class="modal-body">
					<form class="form-horizontal" id="edit_tag_form">
						<input type="hidden" id="edit_tag_id" name="tag_id"/>
						<div class="form-group">
							<label for="id" class="col-sm-2 control-label">id</label>
							<div class="col-sm-10">
								<input type="text" readonly="true" class="form-control" id="id" placeholder="id" name="id" ng-model=(id)>
							</div>
						</div>															
						<div class="form-group">
							<label for="tag" class="col-sm-2 control-label">tag</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="tag" placeholder="tag" name="tag">
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="button" class="btn btn-primary" ng-click="updatetag()">保存修改</button>
				</div>
			</div>
		</div>    
        
        
        
        
 </div>

<div class="modal fade" id="tagAddDialog" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel">
<div class="modal-content">
  <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	  <span aria-hidden="true">&times;</span>
		</button>
		<h4 class="modal-title" id="myModalLabel">添加tag</h4>
	</div>
<div class="modal-body">
<form class="form-horizontal" id="add_tag_form">

  <input type="hidden" id="edit_tag_id" name="tag_id"/>																					 
	<div class="form-group">
	   <label for="id" class="col-sm-2 control-label">id:</label>
		<div class="col-sm-10">
		<input type="text" class="form-control" id="id" placeholder="id" name="id">
		</div>
	</div>
	<div class="form-group">
	  <label for="tag" class="col-sm-2 control-label">tag:</label>
		<div class="col-sm-10">
		<input type="text" class="form-control" id="tag" placeholder="tag" name="tag">
		</div>
	</div>
</form>
</div>
	<div class="modal-footer">
	  <button type="button" class="btn btn-default" data-dismiss="modal">关闭 
      </button>
	<button type="button" class="btn btn-primary" onclick="addtag()">添加</button>
	</div>
</div>
</div> 



   





<script type="text/javascript">
 var app=angular.module('myApp', []).controller('tagCtrl', function($scope) {
		 $scope.tag_messagelist = ${tag_messageLists};

	 $scope.edittag=function(id){
		 ajaxModule.edittag(id);
	 };
	 $scope.deletetag=function(id){
		 ajaxModule.deletetag(id);
	 };
	 $scope.updatetag=function(){
		 ajaxModule.updatetag();
	 };
	 
});
	var ajaxModule = {
			edittag : function edittag(id) {
				$.ajax({
					type:"GET",
					url:"<%=basepath%>list/edit",
					data:{"id":id},
					success:function(data) {  											
						$("#id").val(data.id);
						$("#tag").val(data.tag);
					},erroe:function(){
						alert('失败');
				}
			});
		},
		deletetag : function deletetag(id) {
			$.ajax({
				type:"POST",
				url:"<%=basepath%>list/delete",
				async:true,
				data:{
					_method:'DELETE',
					"id":id},
				success:function() {
					alert('删除成功');
					window.location.reload();
				},erroe:function(){
					alert('失败');
			}
		});
		},
		updatetag : function updatetag() {
			$.post("<%=basepath%>list/update",
			$("#edit_tag_form").serialize(),function(data){
				alert("tag信息更新成功！");
				window.location.reload();
			});
		}
		
	};

	
	
	function addtag() {
				$.post("<%=basepath%>list/add",$("#add_tag_form").serialize(),function(data){
					alert("tag信息添加成功！");
					window.location.reload();
					});
				}
	

</script>

</body>
</html>